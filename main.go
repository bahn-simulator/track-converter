package main

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Station struct {
	XMLName  xml.Name `xml:"Station"`
	Name     string   `xml:"name,attr"`
	Position float64  `xml:"position,attr"`
}

type Stations struct {
	XMLName  xml.Name  `xml:"Stations"`
	Stations []Station `xml:"Station"`
}

type Configuration struct {
	XMLName xml.Name `xml:"Configuration"`
	Name    string   `xml:"name,attr"`
	Video   string   `xml:"video,attr"`
	Data    string   `xml:"data,attr"`
}

type Route struct {
	XMLName       xml.Name      `xml:"Route"`
	Configuration Configuration `xml:"Configuration"`
	Stations      Stations      `xml:"Stations"`
}

type jsonTrack struct {
	Name     string    `json:"name"`
	Video    string    `json:"videoFile"`
	Data     string    `json:"data"`
	Train    string    `json:"train"`
	Stations []Station `json:"Stations"`
}

func cleanComma(data *[]byte) error {

	if data == nil {
		return errors.New("data cannot be a null pointer")
	}

	for i := 0; i < len(*data); i++ {
		if (*data)[i] == ',' {
			(*data)[i] = '.'
		}
	}

	return nil
}

func main() {
	// Open our xmlFile
	xmlFile, err := os.Open("U6.xml")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Successfully Opened users.xml")
	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = cleanComma(&byteValue)
	if err != nil {
		fmt.Println(err)
		return
	}

	// we initialize our Users array
	var route Route
	// we unmarshal our byteArray which contains our
	// xmlFiles content into 'users' which we defined above
	err = xml.Unmarshal(byteValue, &route)
	if err != nil {
		fmt.Println(err)
		return
	}

	var track jsonTrack
	track.Stations = route.Stations.Stations
	track.Name = route.Configuration.Name
	videoLocNew := strings.ReplaceAll(route.Configuration.Video, filepath.Ext(route.Configuration.Video), ".mkv")
	track.Video = videoLocNew
	dataLocJson := strings.ReplaceAll(route.Configuration.Data, filepath.Ext(route.Configuration.Data), ".json")
	track.Data = dataLocJson
	track.Train = "db433.json"

	jsonString, _ := json.MarshalIndent(track, "", "    ")
	ioutil.WriteFile("U6.json", jsonString, os.ModePerm)

	// we iterate through every user within our users array and
	// print out the user Type, their name, and their facebook url
	// as just an example
	for i := 0; i < len(route.Stations.Stations); i++ {
		station := route.Stations.Stations[i]
		fmt.Println("Station Name: " + station.Name)
		fmt.Println("Station Position: " + strconv.FormatFloat(station.Position, 'f', 5, 64))
	}
}
